# Ansible role - Shibboleth - federated identity service provider #

This role serves for automatic installation of shibboleth on debian-based systems to be used along with apache.

The role will perform basic shibboleth installation. The installation is general and it is expected that the playbook which is using it will connect the shibboleth to its own application in desired way, therefore no apache related settings is done by this role etc.

__IMPORTANT!__ To use shibboleth you first need to generate private key and certificate (`sp-cert.pem` and `sp-key.pem`) using `shib-keygen` command and metadata using `shib-metagen` command, send the metadata to IdP (idp@ics.muni.cz) along with list of requested fields, description of service etc.. Due to this not being possible to automate and not being simple, it is expected that this was already done and those files are available at the time when the playbook using shibboleth for a service is being created!

__IMPORTANT!__ To use shibboleth you also need to use `apache` role because shibboleth uses it as prerequisite.

## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Role paramaters

mandatory

* `shibboleth_entityid` - URI of service provider.
* `shibboleth_idp` - URI of identity provider.
* `shibboleth_idp_metadata` - URI to identity provider metadata.

## Example

Example of the simplest Shibboleth installation.

```yml
roles:
    - role: shibboleth
      shibboleth_entityid: https://example.com/sp/shibboleth
      shibboleth_idp: https://idp2.ics.muni.cz/idp/shibboleth
      shibboleth_idp_metadata: https://id.muni.cz/idp/shibboleth
```

## Application specific tasks ##

Also to finish the installation you might want to do following:

* move `sp-cert.pem` and `sp-key.pem`, `attribute-map.xml` and SP's metadata to `/etc/shibboleth` directory
* enable your service to use shibboleth authentication, such as putting something like this into your apache site config:

    ```apache
    AuthType shibboleth
    ShibRequestSetting requireSession 1
    ```
